import {
    readdir, copyFile,
    lstat, mkdir, exists
} from 'fs';
import { join } from 'path';
import { DEBUG, LtkArray } from '../loft';
export class CopyOperation {
    private ignoring:string[] = [];
    constructor(private from:string, private verbosePrefix?:string, private structureOnly?:boolean) { }
    get structure() {
        this.structureOnly = true;
        return this;
    }
    ignore(...globs:string[]) {
        Array.prototype.push.apply(this.ignoring, globs);
        return this;
    }
    public async to(dest) {
        var operation = this;
        if(!operation.verbosePrefix) operation.verbosePrefix = '';
        return new Promise<any>(function(resolve, reject){
            lstat(operation.from, function (err, info) {
                var thisPathStack = operation.from.split(/\/|\\/g), thisDir = thisPathStack.pop(), patternStack;
                if(!info) return reject(new Error(`Loft can\'t locate: ${operation.from}`))
                if (err) return reject(err);
                //console.log(info)
                //resolve(info);
                //var possibleIgnores = operation.ignoring.filter(pat=>pat.split(/\/|\\/g).pop()===thisDir);
                // looop upwards thorugh a split on / trying to ignore
                // operation.ignoring.filter(
                //     (pattern)=>{
                //         patternStack = pattern.split(/\/|\\/g);
                //         if (patternStack[0]==='**'||patternStack[0]==='*') throw new Error(`Loft won\'t use this glob '${pattern}' (glob symbols must not be at end)`)
                //         for(var i = thisPathStack.length-1; i >= 0; i--) {
                //             if (pattern)
                //         }
                //     }
                // )
                // thisPathStack.forEach((dirOrFile, i)=>{

                // })
                if (info.isDirectory()) {
                    if (thisDir === '.git'||thisDir === 'node_modules') {
                        if(DEBUG) console.info(`⎮ ${operation.verbosePrefix}ℹ️  Skipping .git and node_module at ${operation.from}`);
                        return resolve(true);
                    }
                    console.info(`⎮ ${operation.verbosePrefix}📂  Copying to ${dest}`);
                    exists(dest, function (targetExists) {
                        if(targetExists) reject(new Error('Loft doesn\'t overwrite directories'));
                        mkdir(dest, function (err){
                            readdir(operation.from,"utf8", function (err, list) {
                                if(err) reject(err);
                                //console.log(list);
                                var operations = [];
                                list.forEach(
                                    (item)=> operations.push(
                                        new CopyOperation(
                                                join(operation.from,item),
                                                operation.verbosePrefix + '  ',
                                                operation.structureOnly
                                        )/*.ignore(
                                                operation.ignoring.filter(pat=>pat.split(/\/|\\/g))
                                            )*/.to(
                                                join(dest,item)
                                            )
                                    )
                                );
                                Promise.all(operations).then(done=>resolve(true));
                            });
                        })
                    })
                }
                else {
                    console.info(`⎮ ${operation.verbosePrefix}📄  ${dest} ${operation.structureOnly?'ready for scaffolding...':'copying...'}`);
                    if(!operation.structureOnly)
                        copyFile(operation.from, dest, function(err){
                            if (err) reject(err);
                            resolve(true)
                        });
                    else resolve(true);
                }
            });


        });
    };
}
