import {Loft, LtkArray} from './loft';
export class LoftCommand {
    public name:string; // The unique name of the command
    public icon:string = "⌨️"; // Fun!
    constructor(public args:LoftArguments, public loftInstance?:Loft ) {
    }
    async execute(loft:Loft) {
        
    }
}
export class LoftArguments {
    constructor() { }
    positional:LtkArray<string> = process.argv.slice(2) as any;
}