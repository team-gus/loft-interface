//import * as Handlebars from 'handlebars';
//@loft:XXXXXX
import {
    LoftCommand, Loft, LoftArguments, DEBUG,
    SCAFFOLDS_PATH, SCAFFOLDS,
    AnyHelpCommand, LoftToolkit
//@loft:xxxxxx
} from './any-help-command';
import { join } from 'path';
export class Help extends AnyHelpCommand {
    constructor (public args:LoftArguments, loft?:Loft ) {
        super(args);
    }

    async execute() {

        console.info(`\n❓  Loft Help\n`);
        console.info(`\tloft <help|new> <scaffold|workspace> <target>\n`);
        console.info(`\tloft <new> [workspace] <scaffold-source-name> <target-workspace> [<variable-name> <replace-value>|file:<variable-name> <replace-value>, ...]`);
        console.info(`\t\tCopies 'scaffold' source files to the target-workspace`)
        console.info(``);
        console.info(`\tloft <new> <scaffold> <new-scaffold-name> <scaffold-source>`);
        console.info(`\t\tCopies source files at provided path relative to working directory, into the 'scaffold' source files`)
    }
}
