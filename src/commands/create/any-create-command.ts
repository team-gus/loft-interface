import { Loft, LoftCommand, LoftArguments, DEBUG, LOFT_PATH } from '../../loft';
import { LoftToolkit } from '../../common/loft-toolkit';
import { join } from 'path';
import { readdir } from 'fs';
export const SCAFFOLDS_PATH = join(LOFT_PATH, 'scaffolds');
export const SCAFFOLDS = {};
export class AnyCreateCommand extends LoftCommand{
    constructor (args) {
        super(args);
    }
    name = "create";
    icon = "✨";
    async loadScaffolds() {
        await LoftToolkit.ensure(LOFT_PATH);
        await LoftToolkit.ensure(SCAFFOLDS_PATH);
        return new Promise(
            function (res, rej){
                readdir(SCAFFOLDS_PATH, 'utf8',
                    function (err, info) {
                        if (err) rej(err);
                        info.forEach(dir=>SCAFFOLDS[dir]=join(SCAFFOLDS_PATH,dir));
                        res(SCAFFOLDS);
                    }
                )
            }
        );
    }
}
export {
    Loft, LoftCommand, LoftArguments, DEBUG, LOFT_PATH, LoftToolkit
}