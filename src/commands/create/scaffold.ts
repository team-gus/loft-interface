//import * as Handlebars from 'handlebars';
import {
    LoftCommand, Loft, LoftArguments, DEBUG,
    SCAFFOLDS_PATH, SCAFFOLDS,
    AnyCreateCommand, LoftToolkit
} from './any-create-command';
import { join } from 'path';
export class Scaffold extends AnyCreateCommand {
    constructor (public args:LoftArguments, loft?:Loft ) {
        super(args);
    }

    async execute() {
        console.info(`├ - - - 🗂  scaffold (stored workspace)`);
        var relativeTo = process.cwd(), thisDirName = relativeTo.split('|').pop();
        await this.loadScaffolds();
        var argOffset = 2,
            scaffoldSource = this.args.positional.last() || '.',
            scaffoldName = this.args.positional.first(argOffset) || this.args.positional.first(argOffset-2);
        //console.log(scaffoldName);
        var copyTo = SCAFFOLDS[scaffoldName]
        if (copyTo) throw new Error(`Loft doesn't want to overwrite '${scaffoldName}' (found in ${SCAFFOLDS_PATH})`);
        copyTo = join(SCAFFOLDS_PATH,scaffoldName);
        console.info(`├⎯📂  Copying ${scaffoldSource} into ${copyTo}`);
        var copy = await new LoftToolkit.copy(scaffoldSource).to(copyTo);

        //console.info(`├⎯🔄  Scaffolding code with data...`);
        //Handlebars.compi()
        if (DEBUG==="*"||DEBUG==="data") console.info({});
        //var scaffold = await new LoftToolkit.scaffold(copyFrom, copyTo).with(data);
    }
}