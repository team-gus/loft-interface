//import * as Handlebars from 'handlebars';
import { LoftToolkit } from '../../common/loft-toolkit';
import { join } from 'path';
import { readdir } from 'fs';
import { AnyCreateCommand, LOFT_PATH, Loft, LoftArguments, DEBUG } from './any-create-command';
export const SCAFFOLDS_PATH = join(LOFT_PATH, 'scaffolds');
export const SCAFFOLDS = {};
export class Workspace extends AnyCreateCommand {
    constructor (public args:LoftArguments) {
        super(args);
    }
    async loadScaffolds() {
        await LoftToolkit.ensure(LOFT_PATH);
        await LoftToolkit.ensure(SCAFFOLDS_PATH);
        return new Promise(
            function (res, rej){
                readdir(SCAFFOLDS_PATH, 'utf8',
                    function (err, info) {
                        if (err) rej(err);
                        info.forEach(dir=>SCAFFOLDS[dir]=join(SCAFFOLDS_PATH,dir));
                        res(SCAFFOLDS);
                    }
                )
            }
        );
    }
    async execute() {
        console.info(`├ - - -  📂 workspace`);
        await this.loadScaffolds();
        var argOffset = this.args.positional.first(1).toLocaleLowerCase()==='workspace'?2:1,
            scaffoldName = this.args.positional.first(argOffset);
        argOffset += 1;
        var copyFrom = SCAFFOLDS[scaffoldName],
            copyTo = this.args.positional.first(argOffset);
        if (!copyFrom) throw new Error(`Loft needs a scaffold manifiest for '${scaffoldName}' (none was found in ${SCAFFOLDS_PATH})`)
        var data = {$files:{}};
        argOffset += 1;
        //console.log(process.argv)
        while(argOffset<this.args.positional.length) {
            var arg = this.args.positional.first(argOffset);
            argOffset += 1;
            if(arg.startsWith('file:'))
                data.$files[arg.substr('file:'.length)]=this.args.positional.first(argOffset);
            else data[arg]=this.args.positional.first(argOffset);
            argOffset += 1;
        }
        console.info(`├⎯📂  Copying ${copyFrom} into ${copyTo}`);
        var copy = await new LoftToolkit.copy(copyFrom).structure.to(copyTo);

        console.info(`├⎯🔄  Scaffolding code with data...`);
        //Handlebars.compi()
        if (DEBUG==="*"||DEBUG==="data") console.info(data);
        var scaffold = await new LoftToolkit.scaffold(copyFrom, copyTo).with(data);
    }
}