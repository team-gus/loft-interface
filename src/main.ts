#!/usr/bin/env node
import {Loft, LoftArguments, LoftCommand, DEBUG} from './loft';
import { LtkArray, LtkString } from './common/loft-toolkit';
if(DEBUG==="*"||DEBUG==="source-map") {
    require('source-map-support').install();
    console.info(`├⚙️  Source mapping is on...`);
}
var args = new LoftArguments(),
    command = args.positional.first(),
    commandOptions = Loft.commandMap[command.toLocaleLowerCase()],
    subject = args.positional.second() as LtkString;
if(DEBUG==="*"||DEBUG==="command-map") {
    console.info(`├⚙️  Command map: ${JSON.stringify(Loft.commandMap)}`);
}
//if (!command) throw new Error("Expecting a command like 'create'... Need help?");
if (!commandOptions) {
    command = 'new', subject = 'scaffold' as LtkString;
    commandOptions = Loft.commandMap[command.toLocaleLowerCase()];
}
//if (!subject) throw new Error("Expecting a subject like 'workspace' or 'scaffold'... Need help?")
if (!subject) subject = command as LtkString;
const
    type = commandOptions[subject.toPascalCase()],
    implicitType = (typeof type ==='function') ?
        null :
        commandOptions[
            Object.keys(commandOptions as any).find(
                k=>
                    typeof commandOptions[k]==='function'
            )
        ],
    explicitType = implicitType || type;
if (DEBUG==="*"||DEBUG==="command") console.info(explicitType);
new Loft().execute(
    new (explicitType)(args)
).then(
    done=>console.info(`✅`)
).catch(
    err=>`❌  ${err}`
);

    
