import { join } from 'path';
import { LoftToolkit, LtkArray } from './common/loft-toolkit';
import { LoftCommand, LoftArguments} from './loft-command';
const PACKAGE = require('../package.json'), DEBUG = process.env.LOFT_DEBUG || PACKAGE.debug;console.info(`️\n☁️  Loft ${PACKAGE.version}`);
const LOFT_PATH = join(process.env.USERPROFILE || process.env.HOME, '.loft');
export { PACKAGE, DEBUG, LOFT_PATH };
import * as commands from './commands';
export class Loft {
    constructor(public path?:string) {
        var $path = (process.argv as LtkArray<any>).second().split(/\/|\\/g).filter(p=>p);
        //console.log($path)
        this.bin = $path.pop();
        this.path = join.apply(this.path, $path);
        if(DEBUG) console.info(`├☁️⚡ ️Loft'ing from 📂 ${this.path} ⚙️ ${this.bin} in ${process.cwd()}`)
    }
    bin: string;
    async execute(command:LoftCommand) {
        var path = '';
        console.info(`├👟  Executing ${command.icon}  ${command.name} `);
        try {
            await command.execute(this);
            console.info(`🏁  Complete`);
        }
        catch (e) {
            console.error(`❌  ${e}`)
        }
    }
    static get commandMap() {
        return {
            new: commands.create,
            help: commands.help
        }
    }
}
export { LoftToolkit, LtkArray };
export { LoftCommand, LoftArguments };